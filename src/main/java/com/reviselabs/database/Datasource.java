package com.reviselabs.database;

import com.google.inject.Singleton;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.quirks.PostgresQuirks;

/**
 * Master class for connecting to a database.
 */
@Singleton
public class DataSource extends DataSourceBase implements DatabaseService {

    private Logger logger;
    private String url;
    private String user;
    private String password;

    public DataSource() {
        configureDatabase(ConfigFactory.load());
    }

    public DataSource(Config config) {
        //https://github.com/typesafehub/config#merging-config-trees
        configureDatabase(config.withFallback(ConfigFactory.load()));
    }

    private void configureDatabase(Config config) {
        conf = config;
        logger = LoggerFactory.getLogger(DataSource.class);
        hikari = new HikariConfig();
        url = read("db.url");
        user = read("db.user");
        password = read("db.password");
        hikari.setJdbcUrl(url);
        hikari.setUsername(user);
        hikari.setPassword(password);
        hikari.setMaximumPoolSize(Integer.parseInt(read("hikari.maxPoolSize")));
        hikari.setPoolName("com.reviselabs.database");
        if(url.contains("jdbc:postgresql")) setUpPostgreSql();
        else if(url.contains("jdbc:h2")) setUpH2();
        else if(url.contains("jdbc:sqlserver")) setUpSqlServer();
    }

    private void setUpPostgreSql() {
        logger.info("Connecting to PostgreSQL database @ {}...", url);
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        sql2o = new Sql2o(new HikariDataSource(hikari), new PostgresQuirks());
        logger.info("Connection successful!");
    }

    private void setUpH2() {
        logger.info("Connecting to H2 Database @ {}", url);
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        sql2o = new Sql2o(url, user, password);
        logger.info("Connection successful!");
    }

    private void setUpSqlServer() {
        logger.info("Connecting to Microsoft SQL Server Database @ {}", url);
        System.setProperty("java.net.preferIPv4Stack", "true");
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        sql2o = new Sql2o(new HikariDataSource(hikari));
        logger.info("Connection successful!");
    }

    @Override
    public Connection getConnection() {
        //Remember to use DAO helper methods (fetchOne, fetchMany, executeStatement) to handle auto-closing this connection.
        //Use a try-with-resources block if you must access this method directly.
        return sql2o.open();
    }
}
