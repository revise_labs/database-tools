package com.reviselabs.database.data;

import com.google.inject.Inject;
import com.reviselabs.database.DatabaseService;
import org.sql2o.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin on 6/15/2016.
 */
public abstract class DAO {

    public interface SingleRowQuery<T> { T run(Connection connection, Class<T> type); }

    public interface MultiRowQuery<T> { List run(Connection connection, Class<T> type); }

    public interface SingleRowGenericQuery { Row run(Connection connection); }

    public interface MultiRowGenericQuery { List<Row> run(Connection connection); }

    public interface SqlStatement { void run(Connection connection); }

    protected RowConverter converter = new RowConverter();

    @Inject protected DatabaseService database;

    @SuppressWarnings("unchecked")
    protected <T> T fetchOne(Class<T> returnType, SingleRowQuery query) {
        try(Connection connection = database.getConnection()) {
            return returnType.cast(query.run(connection, returnType));
        }
    }

    @SuppressWarnings("unchecked")
    protected <T> List<T> fetchMany(Class<T> returnType, MultiRowQuery query) {
        try(Connection connection = database.getConnection()) {
            List<T> castRows = new ArrayList<>();
            List rawRows = query.run(connection, returnType);
            query.run(connection, returnType)
                    .forEach(row -> castRows.add(returnType.cast(row)) );
            return castRows;
        }
    }

    protected Row fetchOne(SingleRowGenericQuery query) {
        try(Connection connection = database.getConnection()) {
            return query.run(connection);
        }
    }

    protected List<Row> fetchMany(MultiRowGenericQuery query) {
        try(Connection connection = database.getConnection()) {
            return query.run(connection);
        }
    }

    protected void executeStatement(SqlStatement statement) {
        Connection connection = database.getConnection();
        statement.run(connection);
        connection.close();
    }

}
