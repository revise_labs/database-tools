package com.reviselabs.database.data;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.converters.DateConverter;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.util.*;

/**
 * Created by Kevin on 7/27/2016.
 */
public class Row implements Map<String, Object> {

    transient private Map<String, Object> data;
    transient private ResultSet resultSet;
    transient private ConvertUtilsBean converter;

    public Row() {
        data = new HashMap<>();
        converter = new ConvertUtilsBean();
        converter.register(new DateConverter(null), Date.class);
    }

    public Row(Map<String, Object> data) { this(); this.data = data; }

    public Row(Map<String, Object> data, ResultSet resultSet) {
        this(data);
        this.resultSet = resultSet;
    }

    public <T> T as(Class<T> type) {
        //BeanUtils.populate(instance, data); //NOTE: This will not work for Beans using the Builder pattern on setter values. Be careful.
        //Doing this manually for greater flexibility and to reuse the converter that I already initialized.
        try {
            T instance = type.newInstance();
            Set<String> keys = data.keySet();
            PropertyDescriptor[] props = Introspector.getBeanInfo(type, Object.class).getPropertyDescriptors();
            for(PropertyDescriptor prop : props) {
                String fieldName = prop.getName();
                if(keys.contains(fieldName)) {
                    Object value = data.get(fieldName);
                    Method writer = prop.getWriteMethod();
                    if(writer != null) writer.invoke(instance, converter.convert(value, prop.getPropertyType()));
                }
            }
            return instance;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | IntrospectionException e){
            e.printStackTrace();
            return null;
        }
    }

    public Boolean getBoolean(String label) {
        return getValueAs(label, Boolean.class);
    }

    public Date getDate(String label) {
        return getValueAs(label, Date.class);
    }

    public Double getDouble(String label) {
        return getValueAs(label, Double.class);
    }

    public Float getFloat(String label) {
        return getValueAs(label, Float.class);
    }

    public Integer getInt(String label) {
        return getValueAs(label, Integer.class);
    }

    public Long getLong(String label) {
        return getValueAs(label, Long.class);
    }

    public String getString(String label) {
        return getValueAs(label, String.class);
    }

    public <T> T getValueAs(String key, Class<T> type) {
        if(data.containsKey(key)) {
            return type.cast(converter.convert(data.get(key), type));
        } else return null;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return data.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return data.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return data.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        return data.put(key, value);
    }

    @Override
    public Object remove(Object key) {
        return data.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ?> m) {
        data.putAll(m);
    }

    @Override
    public void clear() {
        data.clear();
    }

    @Override
    public Set<String> keySet() {
        return data.keySet();
    }

    @Override
    public Collection<Object> values() {
        return data.values();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return data.entrySet();
    }


}
