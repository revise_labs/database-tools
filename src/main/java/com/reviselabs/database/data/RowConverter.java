package com.reviselabs.database.data;

import com.reviselabs.database.util.StringConverter;
import org.sql2o.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ksheppard on 27/07/2016.
 */
public class RowConverter implements ResultSetHandler<Row> {

    protected RowConverter() {}

    @Override
    public Row handle(ResultSet resultSet) throws SQLException {
        return resultSetAsRow(resultSet);
    }

    private Row resultSetAsRow(ResultSet resultSet) throws SQLException {
        Map<String, Object> _result = new HashMap<>();
        ResultSetMetaData metaData;
        try {
            metaData = resultSet.getMetaData();
            for(int i = 1; i <= metaData.getColumnCount(); i++) {
                _result.put(StringConverter.toCamelCase(metaData.getColumnLabel(i)), resultSet.getObject(i));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Row(_result, resultSet);
    }

}
