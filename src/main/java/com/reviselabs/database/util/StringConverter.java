package com.reviselabs.database.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ksheppard on 29/07/2016.
 */
public class StringConverter {

    static Pattern pattern;
    static Matcher matcher;

    public static String toCamelCase(String string) {
        //Strip underscores, replace them with no space + the upper case version of whatever letter followed it
        if (string.contains("_")) {
            string = string.toLowerCase();
            pattern = Pattern.compile("_(.)");
            matcher = pattern.matcher(string);
            StringBuffer sb = new StringBuffer();
            while (matcher.find()) {
                matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
            }
            matcher.appendTail(sb);
            return sb.toString();
        }

        //All upper case column names get converted to lowercase
        pattern = Pattern.compile("\\b[A-Z0-9]+\\b");
        if(pattern.matcher(string).matches()) return string.toLowerCase();

        //Anything else means the column name is either already in lower case or camel case
        return string;
    }

    public static String toSnakeCase(String string) {
        Pattern pattern = Pattern.compile("[A-Z]");
        Matcher matcher = pattern.matcher(string);
        StringBuffer buffer = new StringBuffer();
        while(matcher.find()) {
            matcher.appendReplacement(buffer, "_"+matcher.group().toLowerCase());
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }
}
