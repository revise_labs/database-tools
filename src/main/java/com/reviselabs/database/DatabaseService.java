package com.reviselabs.database;

import org.sql2o.Connection;

import java.util.Scanner;

/**
 * Created by Kevin on 6/15/2016.
 * http://www.journaldev.com/2394/java-dependency-injection-design-pattern-example-tutorial
 * http://www.journaldev.com/2403/google-guice-dependency-injection-example-tutorial
 */
public interface DatabaseService {
    Connection getConnection();

    default void loadDataFromResource(String fileName) {
        String sql = "";
        Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream(fileName));
        while(scanner.hasNextLine()) {
            sql += scanner.nextLine();
        }
        getConnection()
                .createQuery(sql)
                .executeUpdate()
                .close();
    }
}
