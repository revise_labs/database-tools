package models;

/**
 * Created by ksheppard on 27/07/2016.
 */
public class Pet {
    private Long id;
    private String name;

    public Pet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
