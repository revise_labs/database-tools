import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.reviselabs.database.DatabaseInjector;
import com.reviselabs.database.data.Row;
import mock.MockDAO;
import mock.MockDatabase;
import models.Pet;
import models.User;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**

 **/

public class RowTest {

    static Injector injector;
    static MockDAO dao;
    static Logger logger = LoggerFactory.getLogger(RowTest.class);
    private Map<String, Object> data = new HashMap<>();
    private Row alex;

    @Before
    public void setUp() throws Exception {
        data.put("firstName", "Alex");
        data.put("lastName", "Green");
        data.put("age", 27);
        data.put("healthy",true);
        data.put("dateOfBirth", new Date());
        data.put("monthlyIncome", 10000.00);
        data.put("drivingRating", (float) 4.5);
        data.put("id", (long) 328);
        alex = new Row(data);
    }

    @BeforeClass
    public static void createInjector() {
        injector = Guice.createInjector(new DatabaseInjector(new MockDatabase()));
        dao = injector.getInstance(MockDAO.class);
    }

    @Test
    public void serializationTest() { //Tests if it serializes to JSON the same way a HashMap does.
        Map<String, Object> data = new HashMap<>();
        data.put("name", "Kevin");
        data.put("age", 27);
        Row row = new Row(data);
        assertTrue(row.containsKey("name"));
        assertTrue(row.containsKey("age"));
        assertNotNull(row.get("name"));
        assertNotNull(row.get("age"));
        assertEquals(row.get("name"), "Kevin");
        assertEquals(row.get("age"), 27);
        logger.debug(new Gson().toJson(row));
    }

    @Test
    public void nullValueTest() {
        Map<String, Object> data = new HashMap<>();
        data.put("name", null);
        data.put("age", null);
        data.put("createdAt", null);
        Row row = new Row(data);
        row.as(User.class);
    }

    @Test
    public void conversionToAnotherClassTest() {
        Row userRow = dao.getUserRow();
        User user = userRow.as(User.class);
        logger.debug(user.getCreatedAt().toString());

        Row petRow = dao.getPetRow();
        Pet fido = petRow.as(Pet.class);
        logger.debug(fido.getName());

        assertNotNull(user);
        assertNotNull(fido);
        assertNotNull(user.getCreatedAt());
        assertNotNull(fido.getId());
    }

    @Test
    public void testGetBoolean() throws Exception {
        assertNotNull(alex.getBoolean("healthy"));
    }

    @Test
    public void testGetDate() throws Exception {
        assertNotNull(alex.getDate("dateOfBirth"));
    }

    @Test
    public void testGetDouble() throws Exception {
        assertNotNull(alex.getDouble("monthlyIncome"));
    }

    @Test
    public void testGetFloat() throws Exception {
        assertNotNull(alex.getFloat("drivingRating"));
    }

    @Test
    public void testGetInt() throws Exception {
        assertNotNull(alex.getInt("age"));
    }

    @Test
    public void testGetLong() throws Exception {
        assertNotNull(alex.getLong("id"));
    }

    @Test
    public void testString() throws Exception {
        assertNotNull(alex.getString("firstName"));
    }
}
