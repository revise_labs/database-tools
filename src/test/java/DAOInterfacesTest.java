import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.reviselabs.database.DatabaseInjector;
import com.reviselabs.database.data.Row;
import mock.MockDAO;
import mock.MockDatabase;
import models.User;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**

 **/

public class DAOInterfacesTest {

    static Injector injector;
    static MockDAO dao;
    static Logger logger = LoggerFactory.getLogger(DAOInterfacesTest.class);
    Row row;
    List<Row> rows;


    @BeforeClass
    public static void createInjector() {
        injector = Guice.createInjector(new DatabaseInjector(new MockDatabase()));
        dao = injector.getInstance(MockDAO.class);
    }

    @Test
    public void fetchOne() {
        User user = dao.findById(1);
        assertNotNull(user);
        logger.debug(user.toString());
    }

    @Test
    public void fetchMany() {
        List<User> userList = dao.findAll();
        assertNotNull(userList);
        assertFalse(userList.isEmpty());
        for(User u : userList) logger.debug(u.toString());
    }

    @Test
    public void executeStatement() {
        User user = dao.findByEmail("kevin@mail.com");
        assertNotNull(user);
        user.setEmail("kshep92@gmail.com");
        dao.update(user);
        user = dao.findByEmail("kshep92@gmail.com");
        assertNotNull(user);
    }

    @Test
    public void getCompositeRow() {
        row = dao.getCompositeUserRow();
        assertNotNull(row);
        assertTrue(row.containsKey("petId"));
        logger.debug("getCompositeRow: {}", new Gson().toJson(row));
    }

    @Test
    public void getCompositeRows() {
        rows = dao.getCompositeUserRows();
        assertFalse(rows.isEmpty());
        logger.debug("There are {} rows", rows.size());
        logger.debug("getCompositeRows: {}", new Gson().toJson(rows));
    }
}
